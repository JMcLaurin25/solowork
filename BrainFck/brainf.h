#ifndef BRAINF_H_
#define BRAINF_H_

#include <iostream>
#include <string>
#include <vector>

class BrainF {
public:
	BrainF(const std::string &init_program);
	void run_program();

private:
	void process_instruction();
	void increment_tape_index(int count = 1);
	void decrement_tape_index(int count = 1);
	int find_closing_bracket(int index);
	void increment_tape_reg();
	void decrement_tape_reg();
	std::string  program;
	std::vector<int> tape;
	int tape_index;
	int reg;
	int program_counter;

};

#endif /* BRAINF_H_ */
