#include <string>
#include <vector>
#include "brainf.h"

int main(){
	//Add 2 numbers
	//std::string program = ",>,<[->+<]>.";
	//Add 3 numbers
	//std::string program = ",>,<[->+<]>>,<[->+<]>.";
	//Add 4 numbers
	//std::string program = ",>,<[->+<]>>,<[->+<]>>,<[->+<]>.";


	//Multiply 2 numbers - Class Multiplication result
	//std::string program = ",>,[<[>>+<<-]>>[>+<<<+>>-]<-]>>.";
	//Multiply 2 numbers - My Result
	//std::string program = ",>,[-<[->>+<<]>>[-<<+>>>+<]<]>>.";

	//Exponent
	//std::string program = ">,<,>[->+>+<<]>>[-<<+>>]<<<-[->>[-<[->>+<<]>>[-<<+>>>+<]<]>>[-<<+>>]<<<<]>>.";

	//Using registers to add variables
	std::string program = ",^>>#[{,}-^]<<^>>#[>}[-{<<+>>}]{<-]<<.>.>.>.>.>.";
	//std::string program = ",^>>#[.{,.<.<.<.<.<.>>>>>}.-.]";


	std::cout << "Running Program: " << program << std::endl;
	BrainF state(program);
	state.run_program();
}

