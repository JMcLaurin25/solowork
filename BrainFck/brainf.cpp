#include <iostream>
#include <string>
#include <vector>
#include "brainf.h"

BrainF::BrainF(const std::string &init_program)
{
	program = init_program;
	tape.push_back(0);
	tape_index = 0;
	reg = 0;
	program_counter = 0;

};

int BrainF::find_closing_bracket(int index)
{
	char instruction = program[index];
	bool forward;
	int balance;
	switch (instruction)
	{
	case '[':
		forward = true;
		balance = -1;
		break;
	case ']':
			forward = false;
			balance = 1;
			break;
	default:
		throw std::string("Not a loop constructor");
	}
	while(balance != 0)
	{
		if (forward) { index++;}
		else { index--;}

		//Jump forward if 0. Step if !=0
		if (program[index] == '[') {balance--;}
		if (program[index] == ']') {balance++;}


	}
	return index;
}

void BrainF::process_instruction()
{
	char instruction = program[program_counter];
	switch(instruction)
	{
		// (+/-) Increments tape at RW head
		case '+':
			tape[tape_index]++;
			break;
		case '-':
			tape[tape_index]--; //Or tape[tape_index] = tape[tape_index] - 1;
			break;

		// (./,) Handles Output/Input
		case '.': //Output
			std::cout << "Number at index " << tape_index << ": " << tape[tape_index] << "\n";
			break;
		case ',': //Input
			std::cout << "Enter number: ";
			std::cin >> tape[tape_index];
			break;
			return;

		// (>/<) Increment Tape index, Using error checking functions
		case '>':
			increment_tape_index();
			break;
		case '<':
			decrement_tape_index();
			break;


		case '[':
			if (tape[tape_index] == 0){
				program_counter = find_closing_bracket(program_counter);
			}
			break;
		case ']':
			if (tape[tape_index] != 0){
				program_counter = find_closing_bracket(program_counter);
			}
			break;

		//Add new registry functions
		case '#'://Copy registry to tape_index
			tape[tape_index] = reg;
			break;
		case '^'://Copy tape_index to registry
			reg = tape[tape_index];
			break;
		case '{'://tape_index + registry value
			increment_tape_index(reg);
			break;
		case '}'://tape_index - registry value
			decrement_tape_index(reg);
			break;

		default:
			throw std::string("Invalid Construction");
		}

		program_counter++;

	return;
}

void BrainF::increment_tape_index(int count) // Increases the size of vector
{
	tape_index += count;
	while (tape_index >= tape.size())
	{
		tape.push_back(0);
	}
};

void BrainF::decrement_tape_index(int count) // Function for error checking if tape_index is below 0
{
	tape_index -= count;
	if (tape_index < 0)
	{
		throw std::string ("Negative Tape Index.");
	}
};

void BrainF::run_program()
{
	while(program_counter < program.size())
	{
		process_instruction();
	}

	return;
}
