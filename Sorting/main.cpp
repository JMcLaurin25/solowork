#include <vector>
#include <iostream>

using std::vector;
using std::cout;
vector<int> reference_vector = { 9,2,4,7,6,3,5,1,8,0 };

void dump_vector(const vector<int> &in)
{
    cout << "Vector: {";
    for (int num : in)
    {
        cout << num << ", ";
    }
    cout << "}\n";
}
void is_sorted(vector<int> &in, std::string type)
{
	for (unsigned int i = 1; i < in.size()-1; i++)
	{
		if(in[i-1] > in[i])
		{
			cout << "Vector not sorted." << std::endl;
		}
	}
	cout << "    Vector sorted with " << type << " method." << std::endl;
}

int swap(vector<int> &in, int indexLess, int indexMore)
{
	int tempVar = in[indexLess];
	in[indexLess] = in[indexMore];
	in[indexMore] = tempVar;
	return 0;
}

void sort_vector(vector<int> &in)
{
	for(unsigned int i = 0; i < in.size()-1; i++)
	{
		for(unsigned int j = 1; j < in.size(); j++)
		{
			if(in[j-1] > in[j])
			{
				swap(in, j-1, j);
			}

		}
	}
}

void sort_selection(vector<int> &in)
{
	for(unsigned int i = 0; i < in.size(); i++)
	{
		int tempNum=in[i];
		int tempIndex=i;

		for (unsigned int j = i + 1; j < in.size(); j++)
		{
			if(in[j] < tempNum)
			{
				tempNum = in[j];
				tempIndex = j;
			}
		}
		swap(in, i, tempIndex);
	}
}


int divideArray(vector<int> &in, int pivotVar, int low, int high)
{
	int leftLimit = low;
	int rightLimit = high;

	while(leftLimit < rightLimit)
	{
		while(pivotVar < in[rightLimit] && rightLimit > leftLimit)
		{
			rightLimit --;
		}
		swap(in, leftLimit, rightLimit);

		while(pivotVar >= in[leftLimit] && leftLimit < rightLimit)
		{
			leftLimit ++;
		}
		swap(in, rightLimit, leftLimit);
	}
	return leftLimit;
}

void sort_quick(vector<int> &in, int low, int high)
{
	int pivotVar = in[low];
	int divideIndex;

	if(high > low)
	{
		divideIndex = divideArray(in, pivotVar, low, high);

		in[divideIndex] = pivotVar;
		sort_quick(in, low, divideIndex - 1);
		sort_quick(in, divideIndex + 1, high);
	}



}

void output(vector<int> &in, std::string type)
{
	sort_vector(in);
	dump_vector(in);
	is_sorted(in, type);

}

int main()
{
    dump_vector(reference_vector);
    cout << std::endl;

    vector<int> tempVectorBubble = reference_vector;
    output(tempVectorBubble, "bubble");

    vector<int> tempVectorSelection = reference_vector;
    output(tempVectorSelection, "selection");

    vector<int> tempVectorQuick = reference_vector;
    output(tempVectorQuick, "quickSort");

}


