// Pointer types and naming form.

// type *var-name;


int		*ip;	// pointer to an integer
double	*dp;	// pointer to a double
float	*fp;	// pointer to a float
char	*ch;	// pointer to a character

// The pointers type is designated by what it is pointing to.

