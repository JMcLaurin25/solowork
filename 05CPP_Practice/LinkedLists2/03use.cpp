#include <iostream>

int main()
{
	int var = 20; 	// original variable declaration
	int *ip;		// pointer variable. No correlation to any address yet.

	ip = &var;		// Store address of var in pointer variable

	std::cout << "Value of var variable: ";
	std::cout << var << std::endl; //Obviously Prints original Variable

	// Print the Address stored in ip pointer variable
	std::cout << "Address stored in ip variable: ";
	std::cout << ip << std::endl; //Prints the address that was set with &var

	//Access the value at the address available in pointer
	std::cout << "Value of *ip variable: ";
	std::cout << *ip << std::endl;

	// Note after run:
	// The use of * declares that the variable ip is to be an address pointer.
	// Therefore the variable *ip goes to the address of the original variable var

	return 0;
}

//Output after run:
//
//Value of var variable: 20
//Address stored in ip variable: 0xbf94f9f8
//Value of *ip variable: 20

