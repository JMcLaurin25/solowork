/*Chapter 1 - Self Test Exercises. Question 7
 * 
 * Write a comple C++ program that reads two whole numbers into two
 * variables of type int and then outputs both the whole number part and
 * the remainder when the first number is divided by the second. This 
 * can be done using the operators / and %.
 */

#include <iostream>
using namespace std;

int main ()
{
    int varX;
    int varY;
    int quotient;
    int remainder=0;
    
    cout << "First number: ";
    cin >> varX;
    cout << "Second number: ";
    cin >> varY;
    
    quotient = varX / varY;
    remainder = varX % varY;
    
    cout << "The quotient of " << varX << "/" << varY << " is: " << quotient;
    cout << " with a remainder of: " << remainder;
    
    return 0;
}
