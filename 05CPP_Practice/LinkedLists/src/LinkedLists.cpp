/*
 * Add A Node
 * X -> Beginning of the List
 * X -> End of the list
 * X Remove the first Entry
 * X PopHead and PopTail
 * X Remove the Last Entry
 * Get the value of any entry
 * Get the size of the list
 * Insert an entry to the list
 * Pop Value At Index
 * Walk the List (print out all values)
 * Return the list as a vector
 * Print the list backwards (for kicks and giggles)
 */

#include <iostream>
#include <new>

using std::cout;
using std::endl;
using std::string;
using std::cin;

struct Node{
	friend class LinkedList;
public:
        Node();
        Node(int value);
        //int printVal();

private:
        int _value;
        Node *_nextNode;
};

Node::Node(){
        _nextNode       = NULL;
        _value          = NULL;
}

Node::Node(int value){
        _nextNode       = NULL;
        _value          = value;
}

class LinkedList{
public:
        LinkedList();
        //~LinkedList();
        void addNodeBegin(int value);
        void addNodeEnd(int value);
        int rmNodeBegin(int value);
        int rmNodeEnd(int value);
private:
        int _length;
        Node *_listHead;
        Node *_listTail;
};

LinkedList::LinkedList(){
        _length   = 0;
        _listHead = NULL;
        _listTail = NULL;

}

void LinkedList::addNodeBegin(int value){

	if (_listHead == NULL){
		Node *newNode = new Node(value);
		_length += 1;
		_listHead = newNode;
		_listTail = newNode;
	}
	else{
		Node *newNode = new Node(value);
		_length += 1;
		newNode->_nextNode = _listHead;
		_listHead = newNode;
	}
}

void LinkedList::addNodeEnd(int value){
	if (_listTail == NULL){
		Node *newNode = new Node(value);
		_length += 1;
		_listHead = newNode;
		_listTail = newNode;
	}
	else{
		Node *newNode = new Node(value);
		_length += 1;
		Node *current = _listTail;
		current->_nextNode = newNode;
		_listTail = newNode;
	}
}

int LinkedList::rmNodeBegin(int value){
    Node();
	if (_listHead == NULL){
		return -1;
	}
	else if(_length == 1){
		_length -= 1;
		int PopHead = _listHead->_value;
		delete _listHead;
		_listHead = NULL;
		_listTail = NULL;
		return PopHead;
	}
	else{
		_length -= 1;
		Node *current = _listHead->_nextNode;
		int PopHead = _listHead->_value;
		delete _listHead;
		_listHead = current;
		return PopHead;
	}
}

int LinkedList::rmNodeEnd(int value){
    Node();
	if (_listHead == NULL){
		return -1;
	}
	else if(_length == 1){
		_length -= 1;
		int PopHead = _listHead->_value;
		delete _listHead;
		_listHead = NULL;
		_listTail = NULL;
		return PopHead;
	}
	else{
		_length -= 1;
		int PopTail = _listTail->_value;
		Node *top = _listHead;

		while (_listTail != top->_nextNode){
			top = top->_nextNode;
		}

		delete _listTail;
		_listTail = top;
		top->_nextNode = NULL;
		return PopTail;
	}
}

int main(int argc, char* argv[])
{
}
