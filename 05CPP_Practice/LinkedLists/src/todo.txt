/*
 * Add A Node
 * X -> Beginning of the List
 * X -> End of the list
 * X Remove the first Entry
 * X PopHead and PopTail
 * X Remove the Last Entry
 * Get the value of any entry
 * Get the size of the list
 * Insert an entry to the list
 * Pop Value At Index
 * Walk the List (print out all values)
 * Return the list as a vector
 * Print the list backwards (for kicks and giggles)
 */
 