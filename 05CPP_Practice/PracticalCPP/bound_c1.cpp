/* This method aborts an error, but does not resolve anything
 * Problem still needs to be corrected.
 */

#include <iostream>

/* The assert statement tells C++, "This can never happen,
 * but if it does, abort the program in a nice way."
 */
#include <cassert>

const int N_PRIMES = 7; // Number of primes
//The first few prime numbers
int primes[N_PRIMES] = {2, 3, 5, 7, 11, 13, 17};

int main()
{
	int index = 10;

	assert(index < N_PRIMES);
	assert(index >= 0);
	std::cout << "The tenth prime is " << primes[index] << "\n";
	return 0;
}
