//Temperature conversion

#include <string>
#include <iostream>

//Final output
void output(double tempOut, std::string unit)
{
	std::cout << "The converted temperature is: " << tempOut << unit << std::endl;
};

//Conversion function TO FAHRENHEIT
void fahr(double celsIn)
{
	double fahrOut = (9.0 / 5) * celsIn + 32;
	output(fahrOut, "F");
};

//Conversion function TO CELSIUS
void cels(double fahrIn)
{
	double celsOut = (5.0 / 9) * fahrIn - 32;
	output(celsOut, "C");
};

//User interface
int interface()
{
	int selection;

	std::cout << "\t\t+--------------------------------+" << std::endl;
	std::cout << "\t\t|     Temperature Converter      |" << std::endl;
	std::cout << "\t\t|          Select (1)            |" << std::endl;
	std::cout << "\t\t|                                |" << std::endl;
	std::cout << "\t\t|   1. Fahrenheit to Celsius     |" << std::endl;
	std::cout << "\t\t|   2. Celsius to Fahrenheit     |" << std::endl;
	std::cout << "\t\t+--------------------------------+" << std::endl;
	std::cin >> selection;
	return selection;
};


int main()
{
	std::string unit;
	double temperature;
	int convSel = interface();

	//To Celsius
	if (convSel == 1)
	{
		std::cout << "Provide Fahrenheit temperature: " << std::endl;
		std::cin >> temperature;
		cels(temperature);
	}

	//To Fahrenheit
	else if (convSel == 2)
	{
		std::cout << "Provide Celsius temperature: " << std::endl;
		std::cin >> temperature;
		fahr(temperature);
	}

	else
	{
		std::cout << "Please provide either 1 or 2 as a selection." << std::endl;
		interface();
	}

	//output(tempOut, unit);
	return 0;
};
