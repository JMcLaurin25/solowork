/* C String Copy. Combining two strings using strcpy and strcat
 * Utilizing arrays with strings.
 */

#include <cstring>
#include <iostream>

char first[50];	//first name with 50 character spaces.
char last[50];	//last name with 50 character spaces.
char full[75];	//Full name with 75 character spaces.

int main()
{
	std::strcpy(first, "Santa");	//initializes first name
	std::strcpy(last, "Clause");	//initializes last name

	std::strcpy(full, first);	//Adds first name variable to full

	//Now using strcat to add variables to initialized variable
	std::strcat(full, " ");
	std::strcat(full, last);

	std::cout << "His full name is: " << full << std::endl;
	return 0;
}
