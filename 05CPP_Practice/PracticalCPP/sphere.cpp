// Write a program to calculate the volume of a sphere.

#include <iostream>
/*
double convRadius(double radiusIn)
{
	double r;

	return r;
};
*/


//Calculation function for volume
double calcVolume(double r)
{
	double outVolume;
	double pi = 3.14;
	outVolume = 4.0 / 3 * pi * (r * r * r);
	return outVolume;
}

//User interface
double interface()
{
	double radius;

	std::cout << "\t\t+--------------------------------+" << std::endl;
	std::cout << "\t\t|    To calculate the volume     |" << std::endl;
	std::cout << "\t\t|      of a sphere, please       |" << std::endl;
	std::cout << "\t\t|   provide the spheres radius.  |" << std::endl;
	std::cout << "\t\t|          (In inches)           |" << std::endl;
	std::cout << "\t\t+--------------------------------+" << std::endl;
	std::cin >> radius;
	return radius;
};

int main()
{
	double radiusIn = interface();

	double result = calcVolume(radiusIn);

	std::cout << "The resulting volume of radius with a sphere of " << radiusIn << " radius. Is: " << result << std::endl;
	return 0;
}
