/*Chapter 1 - Self Test Exercises. Question 8
 * 
 * Given the following fragment that purports to convert from degrees 
 * Celsius to degrees Fahrenheit:
 * 
 * double c = 20;
 * double f;
 * f = (9/5) * c + 32.0;
 * 
 * What is the value for f?
 * Rewrite it as it was intended
 */


#include <iostream>
using namespace std;

int main()
{
    double c = 20;
    double f;
    f = (9/5) * c + 32.0;
    
    cout << "With celsius provided at: " << c << " the temperature in faahrenheit is: " << f;
    
    return 0;
}
