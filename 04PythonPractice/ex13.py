#pass variables to a script

#import function
from sys import argv

#declare variables for import
script, first, second, third = argv

print "The script is called:", script
print "Your first variable is:", first
print "Your second variable is:", second
print "Your third variable is:", third
