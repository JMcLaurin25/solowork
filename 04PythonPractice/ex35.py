from sys import exit

#function declarations
def gold_room():
    print "This room is full of gold. How much do you take?"
    
    next = raw_input("> ")
    if "0" or "1" in next:
        how_much = int(next)
    else:
        dead("Man, learn to type a number.")
        
    if how_much < 50:
        print "Nice, you aren't too greedy."
        exit(0)
    else:
        dead("You greedy bastard!")
        
def bear_room():
    print "There is a bear in here. Good Choice!"
    print "The bear has a bunch of honey. Not sure how he got it."
    print "The fat bear is in front of another door."
    print "How are you going to move the bear?"
    bear_moved = False
    
    while True:
        next = raw_input("> ")
        
        if next == "take honey":
            dead("The bear looks at you then slaps your face off.")
        elif next == "taunt bear" and not bear_moved:
            print "The bear has moved from the door. you can go through it now."
            bear_moved = True
        elif next == "open door" and bear_moved:
            gold_room()
        else:
            print "I don't have a clue what you want."
            
def cthulhu_room():
    print "Here you see the great evil Cthulhu."
    print "He, it, what it is, stares at you and go insane. That sucks!"
    print "Do you run for you life or eat your hand?"
    
    next = raw_input("> ")
    
    if "run" in next:
        start()
    elif "eat hand" in next:
        dead("Well! That was tasty!")
    else:
        cthulhu_room()
        
def dead(why):
    print why, "Good Job!\nStart over? y/n"
    
    cont = raw_input("> ")
    
    if cont == "y":
        start()
    elif cont == "n":
        exit(0)
    else:
        dead()
    
def start():
    print "You are in a dark room."
    print "There is a door to your right and left."
    print "Which door do you take?"
    
    next = raw_input("> ")
    
    if next == "left":
        bear_room()
    elif next == "right":
        cthulhu_room()
    else:
         start()
         
         
#initiate program based off of functions
start()   
