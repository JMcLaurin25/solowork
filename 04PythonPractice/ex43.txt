Game creation:
    "Gothons from Planet Percal #25"

Synopsis:
"Aliens have invaded a space ship oand our hero has to go through a maze
 of rooms defeating them so he can escape into an escape pod to the 
 planet below. The game will be more like a Zork or Adventure type game 
 with text outupts and funny ways to die. The game will involve an 
 engine that runs a map full of rooms or scenes. Each room will print 
 its own description when the player enters it and then tell the engine 
 what room to run next out of the map."

------------------------------------------------------------------------
Scene Description:
Death
    A humorous method for the player to die.
Central Corridor
    Starting point, which has a Gothon already standing there. The 
    player has to defeat it with a joke, to move on.
Laser Weapon Armory
    Where the hero gets a neutron bomb to blow up the ship before 
    getting to the space pod. It has a keypad he has to guess the number
    for.
The Bridge
    Battle scene with a Gothon. Also the location that the hero places 
    the bomb.
Escape Pod
    Where the hero escapes after guessing the correct escape pod.
    
Key Concepts: (Nouns and Verbs that can be used for class creation.)
    Alien
    Player
    Ship
    Maze
    Room
    Scene
    Gothon
    Escape Pod
    Planet
    Map
    Engine
    Death
    Central Corridor
    Laser Weapon Armory
    The Bridge
    -next_scene
    -opening_scene
    -play
    -enter
    
------------------------------------------------------------------------
Class Hierarchy/Creation:
    * Map
      - next_scene
      - opening_scene
    * Engine
      - play
    * Scene
      - enter
      * Death
      * Central Corridor
      * Laser Weapon Armory
      * The Bridge
      * Escape Pod
