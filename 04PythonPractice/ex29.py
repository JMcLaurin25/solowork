#If statements

#variable declarations
people = raw_input("Insert value for people:")
cats = 30
dogs = 15

#if statements for people
if people < cats:
    print "There are too many cats."
    
if people > cats:
    print "There are too many people."
    
if people < dogs:
    print "You have the right amount of dogs."
    
if people > dogs:
    print "You have too many people"
    

dogs += 5 #adds 5 to dogs variable

#if statments for people and dogs

if people <= dogs:
    print "There are less than or equal people to dogs."
    
if people >= dogs:
    print "There are more than or equal people to dogs."
    
if people == dogs:
    print "There are equal people to dogs." 
