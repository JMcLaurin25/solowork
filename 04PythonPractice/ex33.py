#while loops
#use while loops sparingly. Typically, for-loops are better.

i = 0
numbers = []

while i < 6:
    print "At the top i is %d" % i
    numbers.append(i)
    
    i = i + 1
    print "Numbers now: ", numbers
    
    print "At the bottom i is %d" % i
    
print "The Numbers: "

for num in numbers: #Prints all variables that have been stored in the numbers list
    print num
    
print "This is: %d" % numbers[0]
