#combining arguments with raw input

#establish system function
from sys import argv

#import variable declaration
script, user_name = argv
prompt = '> '

#output
print "Hi %s, I'm the %s script." % (user_name, script)
print "I'd like to ask you a few questions."
print "Do you like me %s?" % user_name
likes = raw_input(prompt)

print "Where do you live %s?" % user_name
lives = raw_input(prompt)

print "What kind of computer do you have?"
computer = raw_input(prompt)

#feedback ouput
print """
Alright, so you said %r about liking me.
You live in %r. Not sure where that is. Don't care.
And you have a %r computer. Eh.
""" % (likes, lives, computer)
