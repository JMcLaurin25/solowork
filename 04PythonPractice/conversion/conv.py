
import re

class Conversion(object):
    def begin(self):
        print "\t+------------------||------------------+"
        print "\t|         Conversion Calculator        |"
        print "\t|                                      |"
        print "\t|      1. Length Conversion            |"
        print "\t|      2. Temperature Conversion       |"
        print "\t+------------------||------------------+\n"
        convType = raw_input("\t\tPlease Provide Selection: ")
        
        if convType == "1":
            Len.main()
        elif convType == "2":
            Temp.main()
        else:
            print "\n\t\t/--Unknown value entered.--/\n"

# Temperature
def is_celsius(unit):
        if unit in "cC":
            return True
        return False
def is_fahrenheit(unit):
        if unit in "fF":
            return True
        return Falsepp
class Temperature(object):
    def main(self):
        print "\t+------------------||------------------+"
        print "\t|         Provide either Celsius       |"
        print "\t|             or Fahrenheit.           |"
        print "\t|           (100.0C / 100.0F)          |"
        print "\t+--------------------------------------+\n"
        valueTemp = raw_input("\t\tTemperature: ")
        
        user_input = re.match("^([0-9]*[.]*[0-9]*)(\s)*([a-zA-Z])$", valueTemp) 
        num = float(user_input.group(1))
        unit = user_input.group(3)

        while True:
            if is_celsius(unit):
                numfah = round(num * (9 / 5) + 32, 2)
                print "\t\t%s = %sF" % (valueTemp, numfah)
                wait = raw_input("\n\t\tPress any key to continue: ")
                Temp.enter()
            elif is_fahrenheit(unit):
                numcel = round((num - 32) * 5 / 9, 2)
                print "\t\t%s = %sF" % (valueTemp, numcel)
                wait = raw_input("\n\t\tPress any key to continue: ")
                Temp.enter()
            else:
                print "Incorrect Entry"
                wait = raw_input("\n\t\tPress any key to continue: ")
                Temp.enter()

# Length
def is_inch(unit):
    if unit in "\"inchINCHInchinchesINCHESInchesin":
        return True
    return False
def is_foot(unit):
    if unit in "\'footFOOTFootfeetFEETFeetft":
        return True
    return False
def is_mile(unit):
    if unit in "milesMILESMilesmi":
        return True
    return False
def is_mm(unit):
    if unit in "mmMMmillimetersMillimeters":
        return True
    return False
def is_cm(unit):
    if unit in "cmCMcentimetersCENTIMETERSCentimeters":
        return True
    return False
def is_m(unit):
    if unit in "mMmetersMETERSMeters":
        return True
    return False
def is_km(unit):
    if unit in "kmKMkilometersKILOMETERSKilometers":
        return True
    return False
class Length(object):
    def main(self):
        print "\t+------------------||------------------+"
        print "\t|            Provide Length            |"
        print "\t|              to convert.             |"
        print "\t+--------------------------------------+\n"
        valueLen = raw_input("\t\tLength: ")
        
        user_input = re.match("^([0-9]*[.]*[0-9]*)(\s)*([a-zA-Z]+)$", valueLen)
        num = float(user_input.group(1))
        unit = user_input.group(3)
        
        # Input conversion to base value for output conversion
        if is_inch(unit):
            numbase = 2.54
            cm = num * numbase
        elif is_foot(unit):
            numbase = 30.48 # 12 * 2.54
            cm = num * numbase
        elif is_mile(unit):
            numbase = 160934.4 # 5280 * 30.48(foot conversion)
            cm = num * numbase
        elif is_mm(unit):
            numbase = 0.1 # 10mm = 1 cm
            cm = num * numbase
        elif is_cm(unit):
            cm = num
        elif is_m(unit):
            numbase = 100 # 1meter = 100cm
            cm = num * numbase
        elif is_km(unit):
            numbase = 100000 # 1km = 1000m (100 * 1000)
            cm = num * numbase
        else:
            print "Incorrect Entry"
            wait = raw_input("\n\t\tPress any key to continue: ")
            Len.enter()

        inch = cm / 2.54
        ft = cm / 30.48
        mile = cm / 160934.4
        mm = cm * 10
        meter = cm / 100
        km = cm / 100000
        
        print "\t+--------------Length Conversion---------------+"
        print "\t|   mm = {:12.2f}       in = {:12.2f}  |".format(mm, inch)
        print "\t|   cm = {:12.2f}       ft = {:12.2f}  |".format(cm, ft)
        print "\t|    m = {:12.2f}     mile = {:12.2f}  |".format(meter, mile)
        print "\t|   km = {:12.2f}                          |".format(km)
        print "\t+----------------------||----------------------+"

#----Call the class and initiate the program----|
Call = Conversion()
Len = Length()
Temp = Temperature()
Call.begin()
