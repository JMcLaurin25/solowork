#Variable Declarations
x = 100
y = 4.0
a = 30
b = 90
c = x - a
d = a
z = d * y
t = b / d

#Output
print "There are", x, "cars available."
print "There are only", a, "drivers available."
print "There will be", c, "empty cars today."
print "We can transport", z, "people today."
print "We have", b, "to carpool today."
print "We need to put about", t, "in each car."
