from sys import argv

script, input_file = argv


#---------function definitions-----------------

def print_all(f):
    print f.read()
    
def rewind(f):
    f.seek(0)
    
def print_a_line(line_count, f):
    print line_count, f.readline()

#----------------------------------------------    
    
#variable definition
current_file = open(input_file)


#----------print outputs-----------------------
print "First let's print the whole file:\n"

print_all(current_file)
print "Now let's rewind, kind of like a tape."

rewind(current_file)#utilizes function defined above

print "Let's print three lines:"

current_line = 1
print_a_line(current_line, current_file)#utilizes function defined above

current_line = current_line + 1
print_a_line(current_line, current_file)#utilizes function defined above

current_line = current_line + 1
print_a_line(current_line, current_file)#utilizes function defined above
