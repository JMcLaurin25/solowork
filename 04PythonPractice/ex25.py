#More Practice
#function declarations

def break_words(stuff):
    #breaks up words
    words = stuff.split(' ')
    return words
    
def sort_words(words):
    #sorts words
    return sorted(words)

def print_first_words(words):
    #prints the first word after popping it off with 0
    word = words.pop(0)
    print word

def print_last_words(words):
    #prints the last word after popping it off with -1
    word = words.pop(-1)
    print word
    
def sort_sentence(sentence):
    #sorts words in a sentence
    words = break_words(sentence)#utilizes the break_words function
    return sort_words(words)
    
def print_first_and_last(sentence):
    #prints first and last words
    words = break_words(sentence)#utilizes the break_words function
    print_first_words(words)
    print_last_words(words)
    
def print_first_and_last_sorted(sentence):
    #this sorts the words and then prints first and last
    words = sorted_sentence(sentence)
    print_first_words(words)
    print_last_words(words)
