# Create classes based upon project development findings listed in ex43.txt

# Basic imports for the game
from sys import exit
from random import randint

#-------------------------------------------------------------------------------------------

        
class Engine(object):
    #Define function for engine actions.
    def __init__(self, scene_map):
        #--------
        self.scene_map = scene_map
    def play(self):
        #-------- Uses functions from Map() class --------
        current_scene = self.scene_map.opening_scene()
        
        #pretty amazing infinite loop
        while True:
            print "\n --------"
            next_scene_name = current_scene.enter()
            current_scene = self.scene_map.next_scene(next_scene_name)

class Scene(object):
    #Define functions for scenes.
    def enter(self):
        print "This is a base Scene class. Nothing in it is configured yet. Create a subclass of it and implement enter()."
        exit(1)#Python parses the classes at runtime, so by putting this outside of the function, it was causing the interpreter to immeadiatly exit.     
            
#Create differing instances of the Scene Class. All instances will have enter function.
class Death(Scene):
    quips = [
        "You died. You suck at this.",
        "Your mom would be proud... if she were smarter.",
        "Such a loser.",
        "Did you even try?"]
    def enter(self):
        print Death.quips[randint(0, len(self.quips)-1)]
        exit(1)        
    
class CentralCorridor(Scene):
    def enter(self):
        #Set the scene
        print "\tThe Gothons of Planet Percal #25 have invaded your ship and destroyed"
        print "your entire crew. You are the last surviving member and your last"
        print "mission is to get the neutron destruct bomb from the Weapns Armory,"
        print "put it in the bridge, and blow the ship up after getting into an "
        print "escape pod."
        print "\n"
        print "You're running down the central corridor to the Weapons Armory when"
        print "a Gothon jumps out, red scaly, skin, dark grimy teeth, and evil clown constume"
        print "flowing around his hate filled body. He's blocking the door to the"
        print "Armory and about to pull a weapon to blast you."    
    
        action = raw_input("> \(shoot, dodge, or tell a joke\)")
    
        if action == "shoot":
            print "\n\n\n\tQuick on the draw you yank out your blaster and fire it at the Gothon."
            print "His clown costume is flowing and moving around his body, which throws"
            print "off your aim. Your laser hits his costume but misses him entirely. This"
            print "completely ruins his brand new costume his mother bought him, which"
            print "makes him fly into a rage and blast you repeatedly in the face until"
            print "you are dead. Then he eats you."
            return 'central_corridor'
        elif action == "dodge":
            print "\n\n\n\tYou dodge the ugly fool and attempt to shoot back."
            print "He laughs at you are rips off your limbs and eats them."
            print "You don't live much longer."
            return 'central_corridor'
        elif action == "tell a joke":
            print "\n\n\n\tYou tell the Gothon a \'Yo momma\' Joke."
            print "He pees his pants from laughter and farts a little."
            print "You get to pass because he can't stop laughing and farting."
            return 'laser_weapon_armory'
        else:
            print "\n\n\n\tEnter one of the three options. STUPID!"
            return 'central_corridor'
    
class LaserWeaponArmory(Scene):
    def enter(self):
        print "\tYou dive into the laser weapon armory and look around for"
        print "more Gothons. There aren't any. They are stupid too. The "
        print "neutron bomb is in the corner with a single digit key code."
        print "\n Don't forget that they are as smart as you are.\n"
        print "You can only guess up to 10 times."
        code = "%d" % (randint(0,10))
        guess = raw_input("\t[Keypad Guess] ")
        guesses = 0
        
        wrong_response = [
        "\tNOPE! HAHA, YOU WRONG!",
        "\tWOW, are you even trying?",
        "\tHAHA! Such a loser.",
        "\tWas that even a number?"]
        
        
        #count the guesses
        while guess != code and guesses < 10:
            print wrong_response[randint(0, len(wrong_response)-1)]
            guesses += 1
            guess = raw_input("\t[Keypad Guess: %d] " % guesses)
        if guess == code:
            print "\n\n\n\tGOT IT. The box opens and you grab the bomb. Now run"
            print "to the bridge and place the bomb where you need to."
            return 'the_bridge'
        else:
            print "\n\n\n\tFOR REAL!? You couldn't guess the #?\n"
            print "The lock buzzes one last time. Poor luck."
            print "You failed to open the box and now you're useless."
            print "The gothons find you and start laughing, as each one"
            print "enjoys eating one of your limbs."
            print "\n\tTRY AGAIN!!!!\n"
            return 'laser_weapon_armory'
    
class TheBridge(Scene):
    def enter(self):
        print "\tYou burst onto the Bridge with the bomb and have no clue"
        print "where to place it. Luckily, no one heard your loud entry"
        print "and now you have a chance to place the bomb. There are a"
        print "group of the Gothon, you can either \"throw\" the bomb at"
        print "them or \"place\" the bomb quietly and run."
        
        action = raw_input("throw or place? > ")
        if action == "throw":
            print "\n\n\n\tHAHAHA!, you're an idiot! The bomb kills them and"
            print "yourself in the process. Smooth move."
            return 'the_bridge'
        
        elif action == "place":
            print "\n\n\n\tYou succeed at not attracting attention and now you have"
            print "to run to the escape pod to get outta here."
            return 'escape_pod'
        else:
            print "\n\n\n\n\n\n\tReally!? You had two options!"
            return 'death'
    
class EscapePod(Scene):
    def enter(self):
        print "\tNow you're rushing through the ship trying to escape"
        print "before the ship blows up. You don't even care that you're"
        print "running like a girl, but you eventually get there. Now"
        print "you need to choose between 2 pods. Which one do you want?"
        
        guess = raw_input("[POD #]: ")
        
        if guess == "1":
            print "\n\n\n\tHoly cow, you actually guessed it. Now you get to"
            print "successfully escape and watch the ship blow up"
            print "with all of the Gothon on board. CONGRATULATIONS!"
            exit (1)
        else:
            print "\n\n\n\tYou chose the wrong one. HAHA, now you get to blow up"
            print "with the Gothon.... At least they aren't eating"
            print "your limbs this time."
            return 'escape_pod'

class Map(object):

    scenes = {'central_corridor': CentralCorridor(), 'laser_weapon_armory': LaserWeaponArmory(),
    'the_bridge': TheBridge(),
    'escape_pod': EscapePod(),
    'death': Death()
    }
    
    #Define functions for actions.
    def __init__(self, start_scene):
        self.start_scene = start_scene

    def next_scene(self, scene_name):
        return Map.scenes.get(scene_name)

    def opening_scene(self):
        return self.next_scene(self.start_scene)

#-------------------------------------------------------------------------------------------
a_map = Map('central_corridor')
a_game = Engine(a_map)
a_game.play()

#-------------------------------------------------------------------------------------------

