#loops and lists

#list example:-----------------------
#hairs = ['brown', 'blonde', 'red']
#eyes = ['brown', 'blue', 'green']
#weights = [1, 2, 3, 4]
#------------------------------------

#lists
the_count = [1, 2, 3, 4, 5]
fruits = ['apples', 'oranges', 'pears', 'apricots']
change = [1, 'pennies', 2, 'dimes', 3, 'quarters']

#first kind of for loop
for number in the_count:
    print "This is count %d" % number
    
#now for fruits
for fruit in fruits:
    print "A fruit of type: %s" % fruit
    
#now for change (mixed list)
for i in change:
    print "I got %r" % i
    

#now build a list
elements = []

#Then use the range function
for i in range (0, 6):
    print "Adding %d to the list." % i
    #append adds to the list
    elements.append(i)
    
#NOW we print elements
for i in elements:
    print "Element was: %d" % i
