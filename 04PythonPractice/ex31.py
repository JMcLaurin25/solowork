#choices and decisions

print "You enter a dark room with two doors. Do you go through Door #1 or Door #2?"

door = raw_input("> ")

if door == "1":
    print "There's a giant bear eating cheesecake. What do you do?"
    print "1. Take the cake"
    print "2. Scream at the bear"
    print "3. Slap the bear"
    
    bear = raw_input("> ")
    
    if bear == "1":
        print "Ha you took a bears food. You're dead now."
    elif bear == "2":
        print "The bear stares at you and the continues eating."
    elif bear == "3":
        print "WOW! The bear bit your head off."
    else:
        print "Well, doing %s, is probably a better choice. Go do that instead." % bear
    
elif door == "2":
    print "You stare into the endless abyss at Cthulhu's retina."
    print "1. Blueberries"
    print "2. Yellow jacket clothespins."
    print "3. Understanding revolvers yelling melodies."
    
    insane = raw_input("> ")
    
    if insane == "1" or insane == "2":
        print "You went insane and stood there forever."
    else:
        print "You don't even know what you're doing, do you?"
