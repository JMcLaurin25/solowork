import pygame
import os
import Tkinter as tk #Used to retrieve the resolution of screen

pygame.init()

class Game():
    def __init__(self):
        self.p1_wins = 0
        self.p2_wins = 0
    
        self.disp_width = 800
        self.disp_height = 600
        self.block_mvmt = 10
        self.block_size = 10
        self.fps = 10
        
        #Establishing Color Palette
        self.white = (255,255,255)
        self.black = (0,0,0)
        self.red = (255,0,0)
        self.ltred = (255,0,51)
        self.orange = (255,128,0)
        self.yellow = (255,255,0)
        self.green = (128,255,0)
        self.blue = (0,0,255)
        self.ltblue = (51,102,255)
        self.grey = (160,160,160)
        
        #Player Colors
        self.p1_Color = self.red
        self.p2_Color = self.blue
        
        
        self.set_win(self.disp_width, self.disp_height)
        #Set parameters for Game display
        self.gameDisplay = pygame.display.set_mode((self.disp_width,self.disp_height))
        self.clock = pygame.time.Clock() #Pulls clock element from pygames library
        pygame.display.set_caption("\"not-TRON!\"")
        
        #Fonts
        self.font = pygame.font.SysFont(None, 25)
        self.font2 = pygame.font.SysFont(None, 100)
        self.font3 = pygame.font.SysFont(None, 20)
        
        self.p1_wins = 0
        self.p2_wins = 0
        
        self.welcome_Screen()
        
    # This function returns the screen resolution
    def get_ScreenRes(self):
        root = tk.Tk()
        screen_width = root.winfo_screenwidth()
        screen_height = root.winfo_screenheight()
        return (screen_width, screen_height)
    
    def set_win(self, winX, winY):
        screen_X, screen_Y = self.get_ScreenRes()
        # Set top left corner of window position
        x = (screen_X - winX) / 2
        y = (screen_Y - winY) / 2
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (x,y)
    
    def message_to_screen(self, font, msg, color, posX, posY):
        msg_length = len(msg) * 4.2
        screen_text = font.render(msg, True, color)
        self.gameDisplay.blit(screen_text, [(self.disp_width/2) - msg_length + posX,(self.disp_height/2) + posY])
    
    def player(self, block_size, color, playerList):
        for pos in playerList:
            pygame.draw.rect(self.gameDisplay,color,[pos[0],pos[1],self.block_size,self.block_size])
    
    def welcome_Screen(self):
        gameExit = False
        while not gameExit:
            self.gameDisplay.fill(self.black)
            self.message_to_screen(self.font2, "\"not-TRON!\"", self.ltblue, -(self.disp_width/4)+50, -(self.disp_height/6))
            self.message_to_screen(self.font, "Press ENTER to Play", self.ltred, 0, -self.block_size)
            
            self.message_to_screen(self.font, "Player 1 controls:", self.grey, -(self.disp_width/4), (self.disp_height/4))
            self.message_to_screen(self.font3, "W, S, A, D", self.grey, -(self.disp_width/4), (self.disp_height/4)+35)
            
            self.message_to_screen(self.font, "Player 2 controls:", self.grey, (self.disp_width/4), (self.disp_height/4))
            self.message_to_screen(self.font3, "UP, DOWN, LEFT, RIGHT", self.grey, (self.disp_width/4)+self.block_size, (self.disp_height/4)+35)
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                        gameExit = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        self.twoPlayer_gameLoop()
            pygame.display.update()
    
    def twoPlayer_gameLoop(self):
        gameExit = False
        gameOver = False
        
        p1_List = []
        p2_List = []
        
        #Head of blocks
        p1_x = self.block_size
        p1_y = self.disp_height / 2
        p1_x_change = self.block_mvmt
        p1_y_change = 0
        p2_x = self.disp_width - self.block_size
        p2_y = self.disp_height / 2
        p2_x_change = -self.block_mvmt
        p2_y_change = 0

        while not gameExit:
            
            while gameOver == True:
                self.gameDisplay.fill(self.black)
                self.message_to_screen(self.font, "PLAYER 1", self.red, -(self.disp_width/4), -(self.disp_height/4))            
                self.message_to_screen(self.font, str(self.p1_wins), self.red, -(self.disp_width/4),-(self.disp_height/4)+35)
                self.message_to_screen(self.font, "PLAYER 2", self.blue, (self.disp_width/4), -(self.disp_height/4))
                self.message_to_screen(self.font, str(self.p2_wins), self.blue, (self.disp_width/4),-(self.disp_height/4)+35)
                self.message_to_screen(self.font, "Game Over, Press N for new match or Q to quit", self.green, 0,(self.disp_height/4))
                
                pygame.display.update()
                
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        gameExit = True
                        gameOver = False
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_q:
                            self.p1_wins = 0
                            self.p2_wins = 0
                            self.welcome_Screen()
                        if event.key == pygame.K_n:
                            self.twoPlayer_gameLoop()
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    gameExit = True
                #PLAYER 1 Controls
                '''if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_a:
                        p1_x_change = -block_mvmt
                        p1_y_change = 0
                    elif event.key == pygame.K_d:
                        p1_x_change = block_mvmt
                        p1_y_change = 0
                    elif event.key == pygame.K_w:
                        p1_y_change = -block_mvmt
                        p1_x_change = 0
                    elif event.key == pygame.K_s:
                        p1_y_change = block_mvmt
                        p1_x_change = 0  
                           
                #PLAYER 2 Controls
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        p2_x_change = -block_mvmt
                        p2_y_change = 0
                    elif event.key == pygame.K_RIGHT:
                        p2_x_change = block_mvmt
                        p2_y_change = 0
                    elif event.key == pygame.K_UP:
                        p2_y_change = -block_mvmt
                        p2_x_change = 0
                    elif event.key == pygame.K_DOWN:
                        p2_y_change = block_mvmt
                        p2_x_change = 0  '''
        
    #NEW PLAYER 1 Controls
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_a:#Left
                        if p1_y_change < 0:# While Moving UP
                            p1_x_change = -self.block_mvmt
                            p1_y_change = 0
                        elif p1_y_change > 0:# While Moving Down
                            p1_x_change = self.block_mvmt
                            p1_y_change = 0
                        elif p1_x_change < 0:# While Moving Left
                            p1_y_change = self.block_mvmt
                            p1_x_change = 0
                        elif p1_x_change > 0:# While Moving Right
                            p1_y_change = -self.block_mvmt
                            p1_x_change = 0
                    elif event.key == pygame.K_d:#Right
                        if p1_y_change < 0:# While Moving UP
                            p1_x_change = self.block_mvmt
                            p1_y_change = 0
                        elif p1_y_change > 0:# While Moving Down
                            p1_x_change = -self.block_mvmt
                            p1_y_change = 0
                        elif p1_x_change < 0:# While Moving Left
                            p1_y_change = -self.block_mvmt
                            p1_x_change = 0
                        elif p1_x_change > 0:# While Moving Right
                            p1_y_change = self.block_mvmt
                            p1_x_change = 0
                    elif event.key == pygame.K_w:# SPEEDS BACK UP TO NORMAL
                        if p1_y_change < 0:# While Moving UP
                            p1_y_change = -self.block_mvmt
                        elif p1_y_change > 0:# While Moving Down
                            p1_y_change = self.block_mvmt
                        elif p1_x_change < 0:# While Moving Left
                            p1_x_change = -self.block_mvmt
                        elif p1_x_change > 0:# While Moving Right
                            p1_x_change = self.block_mvmt
                    elif event.key == pygame.K_s:#SLOWS DOWN TO HALF SPEED
                        if p1_y_change < 0:# While Moving UP
                            p1_y_change = p1_y_change / 4
                        elif p1_y_change > 0:# While Moving Down
                            p1_y_change = p1_y_change / 4
                        elif p1_x_change < 0:# While Moving Left
                            p1_x_change = p1_x_change / 4
                        elif p1_x_change > 0:# While Moving Right
                            p1_x_change = p1_x_change / 4
    #NEW PLAYER 2 Controls
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:#Left
                        if p2_y_change < 0:# While Moving UP
                            p2_x_change = -self.block_mvmt
                            p2_y_change = 0
                        elif p2_y_change > 0:# While Moving Down
                            p2_x_change = self.block_mvmt
                            p2_y_change = 0
                        elif p2_x_change < 0:# While Moving Left
                            p2_y_change = self.block_mvmt
                            p2_x_change = 0
                        elif p2_x_change > 0:# While Moving Right
                            p2_y_change = -self.block_mvmt
                            p2_x_change = 0
                    elif event.key == pygame.K_RIGHT:#Right
                        if p2_y_change < 0:# While Moving UP
                            p2_x_change = self.block_mvmt
                            p2_y_change = 0
                        elif p2_y_change > 0:# While Moving Down
                            p2_x_change = -self.block_mvmt
                            p2_y_change = 0
                        elif p2_x_change < 0:# While Moving Left
                            p2_y_change = -self.block_mvmt
                            p2_x_change = 0
                        elif p2_x_change > 0:# While Moving Right
                            p2_y_change = self.block_mvmt
                            p2_x_change = 0
                    elif event.key == pygame.K_UP:# SPEEDS BACK UP TO NORMAL
                        if p2_y_change < 0:# While Moving UP
                            p2_y_change = -self.block_mvmt
                        elif p2_y_change > 0:# While Moving Down
                            p2_y_change = self.block_mvmt
                        elif p2_x_change < 0:# While Moving Left
                            p2_x_change = -self.block_mvmt
                        elif p2_x_change > 0:# While Moving Right
                            p2_x_change = self.block_mvmt
                    elif event.key == pygame.K_DOWN:#SLOWS DOWN TO HALF SPEED
                        if p2_y_change < 0:# While Moving UP
                            p2_y_change = p2_y_change / 4
                        elif p2_y_change > 0:# While Moving Down
                            p2_y_change = p2_y_change / 4
                        elif p2_x_change < 0:# While Moving Left
                            p2_x_change = p2_x_change / 4
                        elif p2_x_change > 0:# While Moving Right
                            p2_x_change = p2_x_change / 4
        
    
                        
            #Boundary 
            #player 1
            if p1_x >= self.disp_width or p1_x <= 0 or p1_y >= self.disp_height or p1_y <= 0:
                self.p2_wins += 1
                gameOver = True
            #player 2
            if p2_x >= self.disp_width or p2_x <= 0 or p2_y >= self.disp_height or p2_y <= 0:
                self.p1_wins += 1
                gameOver = True
    
            if p1_x == p2_x and p1_y == p2_y:
                gameOver = True
                    
            p1_x += p1_x_change
            p1_y += p1_y_change
            p2_x += p2_x_change
            p2_y += p2_y_change
            
            self.gameDisplay.fill(self.black)
            pygame.draw.rect(self.gameDisplay, self.p1_Color, [p1_x,p1_y,self.block_size,self.block_size])
            pygame.draw.rect(self.gameDisplay, self.p2_Color, [p2_x,p2_y,self.block_size,self.block_size])
            
            #----Builds the trail Player 1
            p1Head = []
            p1Head.append(p1_x)
            p1Head.append(p1_y)
            p1_List.append(p1Head)
            self.player(self.block_size, self.p1_Color, p1_List)
            
            #----Builds the trail Player 2
            p2Head = []
            p2Head.append(p2_x)
            p2Head.append(p2_y)
            p2_List.append(p2Head)
            self.player(self.block_size, self.p2_Color, p2_List)
            
            
            #----Collision Management
            for eachSegment in p1_List[:-1]:
                if p1Head == eachSegment:
                    self.p2_wins += 1
                    gameOver = True
                elif p2Head == eachSegment:
                    self.p1_wins += 1
                    gameOver =True
            for eachSegment in p2_List[:-1]:
                if p1Head == eachSegment:
                    self.p2_wins += 1
                    gameOver = True
                elif p2Head == eachSegment:
                    self.p1_wins += 1
                    gameOver =True
            
            pygame.display.update()
            self.clock.tick(self.fps) # Sets the fps ie-60fps
        
        pygame.quit()
        quit()
        
Game()