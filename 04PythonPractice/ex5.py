#Variable Strings
my_name = 'James W. McLaurin'
my_age = 29 #This is the truth.
my_height = 74 #inches
my_weight = 170 #lbs
my_eyes = 'Brown'
my_teeth = 'White'
my_hair = 'Black'

#Output
print "Let's talk about %s." % my_name
print "He is %d inches tall." % my_height
print "He's %d pounds heavy." % my_weight
print "Actually that's not too heavy."
print "He's got %s eyes and %s hair." %(my_eyes, my_hair)
print "His teeth are usually %s depending on the coffee." % my_teeth

#supposed to be messy syntax to follow
print "If I add %d, %d, and %d I get %d." % (my_age, my_height, my_weight, my_age + my_height + my_weight)
