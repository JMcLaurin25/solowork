#Practice

print "Practice Culmination"

#variable declarations
five = 10 - 2 + 3 - 6
#output
print "This should be five: %d" % five

#function use
def secret_formula(started):
    jelly_beans = started * 500
    jars = jelly_beans / 1000
    crates = jars / 100
    return jelly_beans, jars, crates
    
starting_point = 1000
jelly_beans, jars, crates = secret_formula(starting_point)

#output
print "With a starting point of: %d" % starting_point
print "We have %d jelly beans, %d jars, and %d crates." % (jelly_beans, jars, crates)
