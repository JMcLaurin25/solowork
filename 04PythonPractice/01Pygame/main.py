import pygame
import os
import Tkinter as tk #Used to retrieve the resolution of screen
import random
import time

pygame.init()

disp_width = 800
disp_height = 600
block_mvmt = 10
block_size = 10
apple_size = block_size * 2
fps = 15

#Establishing Color Palette
white = (255,255,255)
black = (0,0,0)
red = (255,0,0)
orange = (255,128,0)
yellow = (255,255,0)
green = (0,128,0)
blue = (0,0,255)
grey = (160,160,160)

# This function returns the screen resolution
def get_ScreenRes():
    root = tk.Tk()
    screen_width = root.winfo_screenwidth()
    screen_height = root.winfo_screenheight()
    return (screen_width, screen_height)

def set_win(winX, winY):
    screen_X, screen_Y = get_ScreenRes()
    # Set top left corner of window position
    x = (screen_X - winX) / 2
    y = (screen_Y - winY) / 2
    os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (x,y)

set_win(disp_width, disp_height)
#Set parameters for Game display
gameDisplay = pygame.display.set_mode((disp_width,disp_height))
pygame.display.set_caption('SNAKEY')

clock = pygame.time.Clock() #Pulls clock element from pygames library
font = pygame.font.SysFont(None, 25)

def message_to_screen(msg, color, posX, posY):
    msg_length = len(msg) * 4
    screen_text = font.render(msg, True, color)
    gameDisplay.blit(screen_text, [(disp_width/2) - msg_length + posX,(disp_height/2) + posY])

def snake(block_size, snakeList):
    for pos in snakeList:
        pygame.draw.rect(gameDisplay, green, [pos[0],pos[1],block_size,block_size])

def apple():
    randAppleX = round(random.randrange(0, (disp_width-block_size))/10.0)*10.0 
    randAppleY = round(random.randrange(0, (disp_height-block_size))/10.0)*10.0
    return (randAppleX, randAppleY)

def gameLoop():
    gameExit = False
    gameOver = False
    
    snakeList = []
    snakeLength = 1
    
    lead_x = disp_width / 2
    lead_y = block_mvmt
    lead_x_change = 0
    lead_y_change = block_mvmt
    appleX, appleY = apple()
    
    while not gameExit:
        
        while gameOver == True:
            gameDisplay.fill(white)
            message_to_screen("Game Over, Press N for new game or Q to quit", red, 0,100)
            pygame.display.update()
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    gameExit = True
                    gameOver = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        gameExit = True
                        gameOver = False
                    if event.key == pygame.K_n:
                        gameLoop()
            
        for event in pygame.event.get():
            #print (event)
            if event.type == pygame.QUIT:
                gameExit = True
            '''if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    lead_x_change = -block_mvmt
                    lead_y_change = 0
                elif event.key == pygame.K_RIGHT:
                    lead_x_change = block_mvmt
                    lead_y_change = 0
                elif event.key == pygame.K_UP:
                    lead_y_change = -block_mvmt
                    lead_x_change = 0
                elif event.key == pygame.K_DOWN:
                    lead_y_change = block_mvmt
                    lead_x_change = 0 '''    
                
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:#Left
                    if lead_y_change < 0:# While Moving UP
                        lead_x_change = -block_mvmt
                        lead_y_change = 0
                    elif lead_y_change > 0:# While Moving Down
                        lead_x_change = block_mvmt
                        lead_y_change = 0
                    elif lead_x_change < 0:# While Moving Left
                        lead_y_change = block_mvmt
                        lead_x_change = 0
                    elif lead_x_change > 0:# While Moving Right
                        lead_y_change = -block_mvmt
                        lead_x_change = 0
                elif event.key == pygame.K_RIGHT:#Right
                    if lead_y_change < 0:# While Moving UP
                        lead_x_change = block_mvmt
                        lead_y_change = 0
                    elif lead_y_change > 0:# While Moving Down
                        lead_x_change = -block_mvmt
                        lead_y_change = 0
                    elif lead_x_change < 0:# While Moving Left
                        lead_y_change = -block_mvmt
                        lead_x_change = 0
                    elif lead_x_change > 0:# While Moving Right
                        lead_y_change = block_mvmt
                        lead_x_change = 0
                elif event.key == pygame.K_UP:# SPEEDS BACK UP TO NORMAL
                    if lead_y_change < 0:# While Moving UP
                        lead_y_change = -block_mvmt
                    elif lead_y_change > 0:# While Moving Down
                        lead_y_change = block_mvmt
                    elif lead_x_change < 0:# While Moving Left
                        lead_x_change = -block_mvmt
                    elif lead_x_change > 0:# While Moving Right
                        lead_x_change = block_mvmt
                elif event.key == pygame.K_DOWN:#SLOWS DOWN TO HALF SPEED
                    if lead_y_change < 0:# While Moving UP
                        while time.time() < (time.time + 5):
                            lead_y_change = lead_y_change / 4
                    elif lead_y_change > 0:# While Moving Down
                        lead_y_change = lead_y_change / 4
                    elif lead_x_change < 0:# While Moving Left
                        lead_x_change = lead_x_change / 4
                    elif lead_x_change > 0:# While Moving Right
                        lead_x_change = lead_x_change / 4

        if lead_x >= disp_width or lead_x < 0 or lead_y >= disp_height or lead_y < 0:
            gameOver = True

        lead_x += lead_x_change
        lead_y += lead_y_change
        
        gameDisplay.fill(white)
        
        pygame.draw.rect(gameDisplay, red, [appleX,appleY,apple_size,apple_size])
        
        # --- Builds the Length of the snake
        snakeHead = []
        snakeHead.append(lead_x)
        snakeHead.append(lead_y)
        snakeList.append(snakeHead)
        snake(block_size, snakeList)
        
        if len(snakeList) > snakeLength:
            del snakeList[0]
        
        # -- Collision Management
        for eachSegment in snakeList[:-1]:
            if eachSegment == snakeHead:
                gameOver = True         
                
        # -- Prevent Apple from generating on Snake
        for eachSegment in snakeList[:-1]:
            if eachSegment >= appleX and eachSegment < (appleX + apple_size):
                if eachSegment >= appleY and eachSegment < (appleY + apple_size):
                    appleX, appleY = apple()
            if (appleX + apple_size) >= disp_width or appleX < 0:
                appleX, appleY = apple()
            elif (appleY + apple_size) >= disp_height or appleY < 0:
                appleX, appleY = apple()
                
        # -- Eaten Apple            
        if lead_x >= appleX and lead_x < (appleX + apple_size):
            if lead_y >= appleY and lead_y < (appleY + apple_size):
                appleX, appleY = apple()
                snakeLength += block_size 

        pygame.display.update()
        clock.tick(fps) # Sets the fps ie-60fps 
    pygame.quit()
    quit()
    
gameLoop()