#Else/If statments

#variable declarations
people = raw_input("How many people? ")
cars = raw_input("How many cars? ")
buses = raw_input("How many buses? ")

if cars > people:
    print "We have too many cars."
elif cars < people:
    print "We don\'t have enough cars."
else:
    print "We don\'t know."
    
if buses > cars:
    print "Way too many buses."
elif cars < buses:
    print "That's expected."
else:
    print "Whats a car?"
