#!/usr/bin/env python

import itertools

class Anagram():
    def __init__(self):
        self.main()
        
    def main(self):
        self.user_in()
        self.parse_input()
        self.comparison()
        self.unique_values()
        self.anagram_out()
        
    def user_in(self):
        print "Welcome to The Anagram Generator"
        print "Please provide a string"
        temp_in = raw_input(": ")
        self.string_in = temp_in.lower()
        
    def parse_input(self):
        self.ana_List = []
        for i in xrange(1, len(self.string_in)+1):
            for combos in itertools.permutations(self.string_in,i):
                self.ana_List.append("".join(combos))    
                
    def comparison(self):
        self.combared_List = []
        #dict_file = open('eng_dict_short.txt', 'r')
        for combo in self.ana_List:
            self.dict_compar(combo)
    
    def dict_compar(self, letter_combo):
        dict_file = open('eng_dict.txt', 'r')
        for line in dict_file.readlines():
            line_data = line.strip()
            if letter_combo == line_data:
                if len(letter_combo) != 1:
                    self.combared_List.append(letter_combo)
                
    def unique_values(self):
        self.final_List = []
        for i in set(self.combared_List):
            self.final_List.append(i)

    def anagram_out(self):
        #print self.combared_List
        print self.final_List
        
Anagram()