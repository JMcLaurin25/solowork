#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;

//Output function Reading in pointer "p"
string Hex(void* p) {
	stringstream Stream;

	Stream << hex << setfill('0') << setw(8) << (long) p;

	return Stream.str();
}

/*
 *void Exercise1(){
 *  cout << "Exercise 1" << endl;
 *
 *	// Enter Exercise Code here.
 *
 *	cin.ignore();
 *	cout << "Exercise 1 Complete" << endl;
 *}
 */

void Exercise1(){
	cout << "Exercise 1" << endl;
  /*
   * Exercise 1: Introduce int variables x and y and int* pointer variables p and q.
   * Set x == 2, y == 8, p to address of x, and q to address of y.
   * Print the following:
   * 	address and value of x
   * 	value of p and *p
   * 	address and value of y
   * 	value of q and * q
   * 	Address of p
   * 	Address of q
   *
   * 	Use Hex function to print pointer address values.
   */

	int x = 2;
	int y = 8;
	int *p = &x;
	int *q = &y;

	cout << "    Address and value of x" << endl;
	cout << "\tx: " << Hex(&x) << " = " << x << endl;

	cout << "    Value of p and *p" << endl;
	cout << "\tp: " << p << " *p: " << *p << endl;

	cout << "    Address and value of x" << endl;
	cout << "\ty: " << Hex(&y) << " = " << y << endl;

	cout << "    Value of q and * q" << endl;
	cout << "\tq: " << q << " *q: " << *q << endl;

	cout << "    Address of p" << endl;
	cout << "\t" << Hex(p) << endl;

	cout << "    Address of q" << endl;
	cout << "\t" << Hex(q) << endl;

	cin.ignore();
	cout << "Exercise 1 Complete" << endl;
}

void Exercise2(){
	cout << "Exercise 2" << endl;

	/*
	 * Introduce int variables x, y, z and int* pntrs p, q, r, s.
	 * Set x, y, z to distinct values.
	 * Set p, q, r to the addresses of z, y, z
	 * 1. Print, with labels, values of x,y,z,p,q,r,*p,*q,*r
	 * 2. Print the message: "Swapping Values"
	 * 3. Execute the swaps:
	 * 		z = x; x = y; y = z;
	 * 4. Print, with labels, values of x,y,z,p,q,r,*p,*q,*r
	 */
	// Enter Exercise Code here.

	int x = 3;
	int y = 4;
	int z = 5;
	int *p = &x;
	int *q = &y;
	int *r = &z;

	cout << "    Values of x,y,z,p,q,r,*p,*q,*r" << endl;
	cout << "\tx:" << x << "\ty:" << y << "\tz:" << z << "\tp:" << p << "\tq:" << q << "\tr:" << r << "\t*p:" << *p << "\t*q:" << *q << "\t*r:" << *r << endl;

	cout << "\n    Swapping Values" << endl;

	z = x;
	x = y;
	y = z;

	cout << "    Values of x,y,z,p,q,r,*p,*q,*r" << endl;
	cout << "\tx:" << x << "\ty:" << y << "\tz:" << z << "\tp:" << p << "\tq:" << q << "\tr:" << r << "\t*p:" << *p << "\t*q:" << *q << "\t*r:" << *r << endl;

	cin.ignore();
	cout << "Exercise 2 Complete" << endl;
}

void Exercise3(){
	cout << "Exercise 3" << endl;

	/*
	 * Introduce int variables x, y, z and int* pointer variables p, q, r.
	 * Set x, y, z to three distinct values.
	 * Set p, q, r to the addresses of x, y, z respectively.
	 * 1. Print with labels the values of x, y, z, p, q, r, *p, *q, *r.
	 * 2. Print the message: Swapping pointers.
	 * 3. Execute the swap code: r = p; p = q; q = r;
	 * 4. Print with labels the values of x, y, z, p, q, r, *p, *q, *r.
	 */

	int x = 3;
	int y = 4;
	int z = 5;
	int *p = &x;
	int *q = &y;
	int *r = &z;

	cout << "    Values of x,y,z,p,q,r,*p,*q,*r" << endl;
	cout << "\tx:" << x << "\ty:" << y << "\tz:" << z << "\tp:" << p << "\tq:" << q << "\tr:" << r << "\t*p:" << *p << "\t*q:" << *q << "\t*r:" << *r << endl;

	cout << "\n    Swapping Values\n" << endl;

	r = p;
	p = q;
	q = r;

	cout << "    Values of x,y,z,p,q,r,*p,*q,*r" << endl;
	cout << "\tx:" << x << "\ty:" << y << "\tz:" << z << "\tp:" << p << "\tq:" << q << "\tr:" << r << "\t*p:" << *p << "\t*q:" << *q << "\t*r:" << *r << endl;

	cin.ignore();
cout << "Exercise 3 Complete" << endl;
}

void Exercise4(){
	cout << "Exercise 4" << endl;

	//Introduce 4 int variables and one array variable a as follows:
	//int x = 11;
	//int y = 12;
	int a[5];
	//int u = 31;
	//int v = 32;

	/*
	 * Then:
	 * 	(1) Write a loop to fill the array a with 21, 22, 23, 24, 25.
	 * 	(2) Execute the following loop:
	 */

	for (int i = 0; i < 5; i++){
		int num1 = 21;
		a[i] = num1 + i;
		num1 += 1;
	}


	for (int i = -2; i < 7; i++){
		cout << setw(2) << i << " " << setw(2) << a[i] << endl;
	}

	/*
	 * Note: The above loop represents very bad practice since it accesses
	 * two cells before the start of the array and two cells after it.
	 * The loop shows that you will access adjacent memory and that C++
	 * gives no direct protection against such memory access errors.
	 */

	cin.ignore();
	cout << "Exercise 4 Complete" << endl;
}

void Exercise5(){
	cout << "Exercise 5" << endl;

	/*
	 * Define an array int a[5] and fill this array with values 1, 4, 7, 10, 13.
	 * Introduce an int i and an int* pointer variable p.
	 * Then run two printing loops:
	 */

	int a[5];
	int i;
	int *p;		cout << endl;

	//Populate a[] with values = 1, 4, 7, 10, 13
	cout << "a[] = ";
	int num1 = 1;
	for (i = 0; i < 5; i++){
		a[i] = num1;
		num1 += 3;
		cout << a[i] << " ";
	}
	 cout << endl;

	//
	for (i = 0; i < 5; i++){
		cout << i << " " << Hex(a) << endl;
		cout << i << " " << Hex(a+i) << " " << a[i] << endl << endl;
	}

	cout << endl;
	i = 0;
	p = a;

	while (p < (a+5)) {
		cout << i << " " << Hex(p) << " " << *p << endl;
		i++;
		p++;
	}

	/*
	 * This exercise shows how a memory address can be manipulated by adding int values to it.
	 */

	cin.ignore();
	cout << "Exercise 5 Complete" << endl;
}


int main (){
	//Exercise1();
	//Exercise2();
	//Exercise3();
	//Exercise4();
	Exercise5();
};
