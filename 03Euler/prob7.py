print "\tBy listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,"
print "\twe can see that the 6th prime is 13."
print "\n\t\tWhat is the 10 001st prime number?"

def main():
    x = raw_input("\n\tWhich sequential prime number do you want? ")
    nthPrime = int(x)
    flag = 1
    primeCount = 0  #Counts the total found prime numbers.
    num = 1    
    
    while primeCount <= nthPrime:
        if is_prime(num):
            primeCount = primeCount + 1
            if nthPrime == primeCount:
                print "\n\tThe sequential prime number (%d) is: %d" % (primeCount, num)
            num = num + 1
        else:
            num = num + 1
        

def is_prime(num):
    i = 1       #Iteration count for primality check
    mod = 0         #Count of modulus calculated for each num
    while i <= num:
        if num % i == 0:
            mod = mod + 1
        i = i + 1
    if mod == 2:
        return True
    else:
        return False
 
main()
