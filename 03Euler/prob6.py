print "\tThe sum of the squares of the first ten natural numbers is,"
print "\t\t12 + 22 + ... + 102 = 385"
print "\tThe square of the sum of the first ten natural numbers is,"
print "\t\t(1 + 2 + ... + 10)^2 = 55^2 = 3025"
print "\tHence the difference between the sum of the squares of the first"
print "\tten natural numbers and the square of the sum is"
print "\t\t3025-385=2640"
print "\n\tFind the difference between the sum of the squares of the first"
print "\tone hundred natural numbers and the square of the sum."

def main():
    value = raw_input("\n\tProvide maximum count: ")
    maxNum = int(value) + 1
    i = 1
    
    x = Sqr_Sum(maxNum)
    y = Sum_Sqr(maxNum)
    difference = x - y
    
    print "\n\t\tSquare of the Sums: %d" % x
    print "\t\tSum of the Squares: %d" % y
    print "\t\t-----------------------------"
    print "\t\t        Difference: %d" % difference

def Sum_Sqr(maxNum):
    totalSumSqr = 0
    for i in range(1, maxNum):
        #Square the number(i)
        currentSqr = i * i
        totalSumSqr += currentSqr
        i += 1
    return totalSumSqr
    
def Sqr_Sum(maxNum):
    totalSqrSum = 0
    preSqr = 0
    for i in range(1, maxNum):
        preSqr += i
        i += 1
    totalSqrSum = preSqr * preSqr
    return totalSqrSum

main()
