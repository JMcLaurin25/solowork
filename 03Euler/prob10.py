print "\tThe sum of the primes below 10 is 2 + 3 + 5 + 7 = 17."
print "\n\tFind the sum of all the primes below two million."


#primelist=[]

def main():
    value = raw_input("\tProvide max boundary: ")
    max_bnd = int(value)
    prime_find(max_bnd)
    #print "\n\t%s - Sum of primes = %d" % (primelist, sum(primelist))
    #print "\n\tSum is: %d" % sum
   
def prime_find(max_bnd):
    sum = 2
    for i in range(3, max_bnd, 2):
            #begin--prime validation----------------------+
            count = 1
            mod = 0
            while count <= i:
                if i % count == 0:
                    mod = mod + 1
                count = count + 1
            if mod == 2:
                #print "\t\t%d" % i
                sum = sum + i
            #end--prime validation------------------------+
    print "\n\tThe sum of primes is: %d" % sum

main()
