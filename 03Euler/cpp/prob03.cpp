/* The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ? */

#include <iostream>
using namespace std;

//Create function to test boolean for primality
bool isprime (int factor){
    int count=1;
    int mod=1;
    while (count <= factor){
        if (factor % count == 0)
            mod += 1;
        count += 1;}
    if (mod == 2)
        return true;
    else
        return false;
    }

int main(int argc, char **argv)
    {
    int factor=1;
    int num=50;
    //long long num=13195;    // Start wih the original they gave. Makes troubleshooting faster.
    //int count=1;

    while (factor < num + 1)
        {
        //int mod;
        //cout << factor << endl;
        
        //Test each factor for primality using the boolean function isprime
        if (isprime (factor))
            cout << "Factor: " << factor << endl;
        factor += 1;}
        
        cout << "Number: " << num << endl;
    return 0;
    }


/* Python Below
 * 
 * factor = 1
 * 
 * #num = 13195
 * num = 600851475143
 * 
 * while factor < num:
 *     if num % factor == 0:
 *         #begin--prime validation----------------------+
 *         count = 1
 *         mod = 0
 *         while count <= factor:
 *             if factor % count == 0:
 *                 mod = mod + 1
 *             count = count + 1
 *         if mod == 2:
 *             print "Prime Factor: %d" % factor
 *         #end--prime validation------------------------+
 *     factor = factor + 1
 */
