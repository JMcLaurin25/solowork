/* "\tA palindromic number reads the same both ways. The largest"
 * "\tpalindrome made from the product of two 2-digit numbers is"
 * "\t9009=91x99"
 * "\n\tFind the largest palindrome made from the product of two"
 * "\t3-digit numbers.\n\n\tSolution:"
 */

#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
using namespace std;

bool isPal(int prod)
{
    string stringConv;
    ostringstream convert;
    convert << prod;
    stringConv = convert.str();
    
    if (stringConv == string(stringConv.rbegin(), stringConv.rend()))
    {
        return true;
    }
    else
    {
        return false;
    }
}

int main()
{
    int maxProd=1;
    for (int i = 999; i >=100; i--)
        {
        for (int j = 999; j >= 100; j--)
            {
            int prod = i * j;

            if (isPal(prod))
                {
                if (prod > maxProd)
                    {
                    maxProd = prod;
                    }
                }
            }
        }
    cout << maxProd << " is a Palindrome" << endl;
}
