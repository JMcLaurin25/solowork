print "\t2520 is the smallest number that can be divided by each of the" 
print "\tnumbers from 1 to 10 without any remainder.\n"
print "\tWhat is the smallest positive number that is evenly divisible"
print "\tby all of the numbers from 1 to 20?\n"

flag = 1
num = 20
while (flag):
    if num % 20 == 0:
        for factor in range(1,21): #Loop Up range to go from 20 to 1
            if factor == 20:
                flag = 0
            #print "Current #: %d\t\tCurrent Factor: %d" % (num, factor)
            if num % factor != 0:
                break
        #print "-----------------------------------"
    num = num + 20
num = num - 1
print "Smallest # divisible by #s 1-20: %d" % num
