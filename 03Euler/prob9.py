print "\tA Pythagorean triplet is a set of three natural numbers, "
print "\ta < b < c, for which,\n"
print "\t\t\ta^2 + b^2 = c^2\n"
print "\tFor example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.\n"
print "\tThere exists exactly one Pythagorean triplet for which\n"
print "\t\t\ta + b + c = 1000.\n"
print "\tFind the product abc.\n"

#using Euclids b = 2mn

def main():
    b = 2
    while True:
        euclid_varb(b)
        b += 2

def euclid_varb(b):
    m = 1
    half_b = b / 2
    while m <= half_b:
        if half_b % m == 0:
            n = half_b / m
            
            #Calculate Values for variables a and c
            a = (m * m) - (n * n)
            c = (m * m) + (n * n)
            if a > 0:
                if c > 0:
                    sum = a + b + c
                    if sum == 1000:
                        print "a=%d, b=%d, c=%d" % (a, b, c)
                        product = a * b * c
                        print "Product = %d" % product
                    
                    return sum
        m = m + 1


main()
