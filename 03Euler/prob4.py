print "\tA palindromic number reads the same both ways. The largest"
print "\tpalindrome made from the product of two 2-digit numbers is"
print "\t9009=91x99"
print "\n\tFind the largest palindrome made from the product of two"
print "\t3-digit numbers.\n\n\tSolution:"

maxdigit1 = 0
maxdigit2 = 0
maxProduct = 0

def palindrome(product):
    prod = str(product)
    if prod == prod[::-1]:
       return True
    else:
       return False

for digit1 in range(100, 1000):
    for digit2 in range(100, 1000):
        product = digit1 * digit2
        if palindrome(product):
            if product > maxProduct:
                maxdigit1 = digit1
                maxdigit2 = digit2
                maxProduct = product
print "\t%d x %d = %d" % (maxdigit1, maxdigit2, maxProduct)

