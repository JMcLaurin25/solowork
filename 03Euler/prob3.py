#The prime factors of 13195 are 5, 7, 13 and 29.
#What is the largest prime factor of the number 600851475143 ?

factor = 1

#num = 13195
num = 600851475143

while factor < num:
    if num % factor == 0:
        #begin--prime validation----------------------+
        count = 1
        mod = 0
        while count <= factor:
            if factor % count == 0:
                mod = mod + 1
            count = count + 1
        if mod == 2:
            print "Prime Factor: %d" % factor
        #end--prime validation------------------------+
    factor = factor + 1
